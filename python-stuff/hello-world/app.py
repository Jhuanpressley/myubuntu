# name = input("What is your name?")
# print("Hello " + name)
# birthYear = input("Enter your birth year")
# age = 2020 - int(birthYear)
# age = str(age)
# print("your age is " + age)

   #calculator
# firstNumb = input("Enter first number")
# secNumb = input("Enter second number")
# sum = float(firstNumb) + float(secNumb)
# sum = str(sum)
# print("Sum = " + sum)

# course = 'Python for Beginners'
# print(course.upper())
# print(course)
# print(course.find('y'))
# print(course.replace('for', '4'))
# print('Python' in course)

# print(10 / 3)
# print(10 // 3)
# print(10 % 3)
# x=10
# print(x)
# x+=3
# print(x)
# x-=4
# print(x)

   #order of operations
# print(10 + 3 * 2)
# print((10+3)*2)

  #comparison operators
# print(10<12)
# print(10>12)
# print(10!=12)
# print(10==12)
#
# price = 5
# print(price == 10 or price ==5)
# print( not price < 6)
# and(both)
# or( at least on)
# not(inverse stout)

     #if condition
# temp = int(input("What temp is it outside?"))
#
# if temp >= 80 :
#     print("it's a hot day out ")
#     print('Drink plenty of water')
# elif temp <= 79 and temp >=65:
#     print("It's a nice day")
# elif temp <= 64:
#     print("It's a bit cold")
#     print("Bring a jacket")
# print("Done")

    #Weight convertor
# weight = float(input("What is your weight?"))
# kgOrLbs = input("is that kilograms (k or K) or pounds (l or L?")
# if kgOrLbs == "l" or kgOrLbs == "L":
#    converted = weight * 0.45
#    print("Weight in Kg: " + str(converted))
# elif kgOrLbs == "k" or kgOrLbs == "K":
#     converted = weight / 0.45
#     print("Weight in Lbs: "+ str(converted))

    #while loops
# i = 1
# while i <=10:
#     print(i * '*')
#     i+=1

   #lists
# names=["Bob","Terra","John","Ashley"]
# print(names[1])
# names[0] = "Fred" #replace index content directly and erases exsisting object
# print(names[0:3]) #prints from - to indexes
# names.append("Bob") #adds to list end
# print(names)
# names.insert(0,"Jack") #inserts new object in mentioned index moves the rest
# print(names)
# print(len(names))

   #loops
# numbers = [1,2,3,4,5]
# for item in numbers:
#     print(item)
#
# i = 0
# while i < len(numbers):
#     i+=1
#     print(i)

   #range
# numbers = range(5)
# for number in numbers:
#     print(number)
#     #OR
# for number in range(5):
#     print(number)

    #tupils
numbers = (1,2,3) #tupils can not be changed
# numbers[0]=2
numb = (1,2,3,3,4,5,6)
print(numb.count(3))
print(numb.index(1,0,6))
