from config import keys
import time
from selenium import webdriver


# incognito window

def order(keys):
    # chrome_options = Options()
    # chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome('./chromedriver')
    driver.get(keys['product_url'])
    
    driver.find_element_by_xpath('//*[@id="add-on-atc-container"]/div[1]/section/div[1]/div[3]/button/span/span').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="cart-root-container-content-skip"]/div[1]/div/div[2]/div/div/div/div/div[3]/div/div/div[2]/div[1]/div[2]/div/button[1]/span').click()
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div/div[1]/div/div/div/div/div[3]/div/div[1]/div/section/section/div/button/span').click()
    time.sleep(1.5)
    driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div/div[2]/div/div[2]/div/div/div/div[3]/div/div/div[3]/button/span').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="firstName"]').send_keys(keys["firstName"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="lastName"]').send_keys(keys["lastName"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="phone"]').send_keys(keys["phone"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="email"]').send_keys(keys["email"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="addressLineOne"]').send_keys(keys["addressLineOne"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="addressLineTwo"]').send_keys(keys["addressLineTwo"])
    time.sleep(1)
    # driver.find_element_by_xpath('//*[@id="city"]').send_keys(keys["city"])
    # time.sleep(1)
    # driver.find_element_by_xpath('//*[@id="state"]').send_keys(keys["state"])
    # time.sleep(1)
    # driver.find_element_by_xpath('//*[@id="postalCode"]').send_keys(keys["postalCode"])
    # time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div/div[3]/div[1]/div[2]/div/div/div/div[3]/div/div/div/div/div/form/div[2]/div[2]/button').click()
    time.sleep(2.5)
    # driver.find_element_by_xpath('//*[@id="firstName"]').send_keys(keys["firstName"])
    # time.sleep(1)
    # driver.find_element_by_xpath('//*[@id="lastName"]').send_keys(keys["lastName"])
    # time.sleep(1)
    driver.find_element_by_xpath('//*[@id="creditCard"]').send_keys(keys["creditCard"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="month-chooser"]').send_keys(keys["month-chooser"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="year-chooser"]').send_keys(keys["year-chooser"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="cvv"]').send_keys(keys["cvv"])
    time.sleep(1)
    # driver.find_element_by_xpath('//*[@id="phone"]').send_keys(keys["phone"])
    # time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div/div[4]/div[1]/div[2]/div/div/div/div[3]/div[2]/div/div/div/div[2]/div/div/div/form/div[3]/div/button').click()
    time.sleep(2)
    driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div/div[2]/div[1]/div[2]/div/div/div[2]/div/form/div/button').click()
    time.sleep(2)



if __name__ == '__main__':
    order(keys)