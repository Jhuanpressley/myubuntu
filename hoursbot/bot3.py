from config import keys
import time
from selenium import webdriver


# incognito window

def order(keys):
    # chrome_options = Options()
    # chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    #driver.maximize_window() ##For maximizing window
    driver.implicitly_wait(20) ##gives an implicit wait for 20 seconds
    driver.get(keys['product_url'])
    
    driver.find_element_by_xpath('//*[@id="DefaultContent_TxtEmailAddress"]').send_keys(keys["email"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_TxtPassword"]').send_keys(keys["pswd"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_btnLog"]').click()
    time.sleep(2)
    # driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/nav/div/ul/li[3]/a').click() #clicks on timesheet in menu
    # #driver.execute_script("window.scrollTo(3,3)")
    # driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div/div[1]/div[1]/div/nav/div/ul/li[3]/ul/li[2]/a').click() #clicks on createtimesheet
    # time.sleep(0.5)
    driver.find_element_by_xpath('//*[@id="landingPage"]/div[1]/a').click()
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="txtGen1"]').send_keys(keys["Name"])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_CboHoursType"]/option[2]').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_CboShifts"]/option[2]').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_btnAddTask"]').click()
    time.sleep(1)
    #monday
    driver.find_element_by_xpath('//*[@id="txtST12"]').click() ##clicks on day timeblock monday
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtStarttime"]').send_keys(keys["st"]) #enters starttime
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtEndTime"]').send_keys(keys["et"])  #enters endtime
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[10]/div/button[1]').click() ##saves timeinput
    time.sleep(1)
    #tuesday
    driver.find_element_by_xpath('//*[@id="txtST22"]').click() ##clicks on day timeblock tues
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtStarttime"]').send_keys(keys["st"]) #enters starttime
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtEndTime"]').send_keys(keys["et"])  #enters endtime
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[10]/div/button[1]').click() ##saves timeinput
    time.sleep(1)
    #wednesday
    driver.find_element_by_xpath('//*[@id="txtST32"]').click() ##clicks on day timeblock weds
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtStarttime"]').send_keys(keys["st"]) #enters starttime
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtEndTime"]').send_keys(keys["et"])  #enters endtime
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[10]/div/button[1]').click() ##saves timeinput
    time.sleep(1)
    #thursday
    driver.find_element_by_xpath('//*[@id="txtST42"]').click() ##clicks on day timeblock thurs
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtStarttime"]').send_keys(keys["st"]) #enters starttime
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtEndTime"]').send_keys(keys["et"])  #enters endtime
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[10]/div/button[1]').click() ##saves timeinput
    time.sleep(1)
    #friday
    driver.find_element_by_xpath('//*[@id="txtST52"]').click() ##clicks on day timeblock fri
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtStarttime"]').send_keys(keys["do"]) #enters starttime
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="txtEndTime"]').send_keys(keys["do"])  #enters endtime
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[10]/div/button[1]').click() ##saves timeinput
    time.sleep(2)
    # driver.find_element_by_xpath('//*[@id="delete2"]').click() ##deletes timesheet
    # time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DefaultContent_btnSubmit"]').click() ##submits timesheet
    time.sleep(1)
    driver.find_element_by_xpath('/html/body/div[12]/div[11]/div/button[1]').click() ## verifys submition of timesheet
    time.sleep(1)


if __name__ == '__main__':
    order(keys)